package com.epam.training.julian_naranjo.taskthree.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GCPCostEstimateSummary {
    private WebDriver driver;
    private String previousPrice;

    @FindBy(xpath = "//h4[@class='n8xu5 Nh2Phe D0aEmf']" )
    private WebElement elementoEnNuevaVentana;

    public GCPCostEstimateSummary(WebDriver driver, String previousPrice) {
        this.driver = driver;
        this.previousPrice = previousPrice;
        PageFactory.initElements(driver, this);
    }

    public boolean validateComputeEnginePriceSummaryPrice(){
        if (previousPrice.equals(elementoEnNuevaVentana.getText())){
            return true;
        }else {
            return false;
        }
    }
}
