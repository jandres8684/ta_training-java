package com.epam.training.julian_naranjo.taskthree.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GCPHomePage {
    private static final String HOMEPAGE_URL ="https://cloud.google.com";
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"kO001e\"]/div[2]/div[1]/div/div[2]/div[1]/div/nav/tab[4]/a[1]")
    private WebElement pricingMenu;

    @FindBy(xpath = "//*[@id=\"kO001e\"]/div[2]/div[6]/div/div/div[2]/ul/li[3]/div/a/div[1]")
    private WebElement selectPricingCalculator;

    public GCPHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public GCPHomePage openPage(){
        driver.get(HOMEPAGE_URL);
        return this;
    }

    public GCPSelectPricingCalculator gcpSelectOptionPricing(){
        pricingMenu.click();
        selectPricingCalculator.click();
        return new GCPSelectPricingCalculator(driver);

    }
}
