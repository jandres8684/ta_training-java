package com.epam.training.julian_naranjo.taskthree.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCPSelectPricingCalculator {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[2]/div[1]/div/div[2]/div/button/span[5]")
    private WebElement pricingCalculator;

    @FindBy(xpath = "//*[@id=\"yDmH0d\"]/div[5]/div[2]/div/div/div/div[2]/div/div/div[1]/div/div/div/h2")

    private WebElement selectComputeEngine;

    public GCPSelectPricingCalculator(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public GCPComputeEnginePricingCalculator gcpSelectPricingCalculator(){
        pricingCalculator.click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(selectComputeEngine));
        selectComputeEngine.click();
        return new GCPComputeEnginePricingCalculator(driver);

    }
}
