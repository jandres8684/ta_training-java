package com.epam.training.julian_naranjo.taskthree.page;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class GCPComputeEnginePricingCalculator {
    private WebDriver driver;
    private String originalWindow;
    private String estimatedCost;

    @FindBy(xpath = "//*[@id=\"c11\"]")
    private WebElement setNumberInstances;

    @FindBy(xpath = "//div[1]/div[3]/div/div/div/div[1]/div")
    private WebElement searchMachineType;

    @FindBy(xpath = "//div[3]/div/div/div/div[2]/ul/li[7]")
    private WebElement selectMachineType;

    @FindBy(xpath = "//div[21]/div/div/div[1]/div/div/span/div/button/div/span[1]")
    private WebElement selectAddGpus;

    @FindBy(xpath = "//div[23]/div/div[1]/div/div/div/div[1]/div")
    private WebElement searchGpuModel;

    @FindBy(xpath = "//div[23]/div/div[1]/div/div/div/div[2]/ul/li[3]")
    private WebElement selectGpuModel;

    @FindBy(xpath = "//div[27]/div/div[1]/div/div/div/div[1]/div")
    private WebElement searchLocalSdd;

    @FindBy(xpath = "//div[27]/div/div[1]/div/div/div/div[2]/ul/li[3]")
    private WebElement selectLocalSdd;

    @FindBy(xpath = "//div[29]/div/div[1]/div/div/div/div[1]/div")
    private WebElement searchRegion;

    @FindBy(xpath = "//li[@data-value='asia-east1']")
    private WebElement regionSelected;

    @FindBy(xpath = "//div[29]/div/div[1]/div/div/div/div[2]/ul/li[5]")
    private WebElement selectRegion;

    @FindBy(xpath = "//div[31]/div/div/div[2]/div/div/div[2]/label")
    private WebElement selectDiscountOptions;

    @FindBy(xpath = "//div[4]/div[1]/div[2]/label")
    private WebElement totalCost;

    @FindBy(xpath = "//div[2]/div[2]/div/button/span[4]")
    private WebElement shareButtom;

    @FindBy(xpath = "//*[@id=\"yDmH0d\"]/div[5]/div[2]/div/div/div/div[1]/a")
    private WebElement estimatedSummaryButtom;

    @FindBy(xpath = "//div[@class='Z7Qi9d HY0Uh']")
    private WebElement priceUpdate;

    public GCPComputeEnginePricingCalculator(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public GCPCostEstimateSummary gcpInstanceConfigurationTotal() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(setNumberInstances));
        setNumberInstances.clear();
        setNumberInstances.sendKeys("4");
        searchMachineType.click();
        selectMachineType.click();
        selectAddGpus.click();
        wait.until(ExpectedConditions.visibilityOf(searchGpuModel));
        searchGpuModel.click();
        selectGpuModel.click();
        searchLocalSdd.click();
        selectLocalSdd.click();
        Actions actions = new Actions(driver);
        actions.scrollToElement(searchRegion).perform();
        searchRegion.click();
        wait.until(ExpectedConditions.elementToBeClickable(selectRegion));
        selectRegion.sendKeys("Ta");
        wait.until(ExpectedConditions.elementToBeClickable(selectRegion));
        selectRegion.click();
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.attributeToBe(regionSelected,"aria-selected","true")));
        selectDiscountOptions.click();
        wait.until(ExpectedConditions.invisibilityOf(priceUpdate));
        estimatedCost = totalCost.getText();
        Assert.assertNotEquals("--",estimatedCost);
        shareButtom.click();
        originalWindow = driver.getWindowHandle();
        wait.until(ExpectedConditions.visibilityOf(estimatedSummaryButtom));
        estimatedSummaryButtom.click();
        Set<String> windowHandles = driver.getWindowHandles();
        String newWindow = null;

        for (String win : windowHandles) {
            if (!win.equals(originalWindow)) {
                newWindow = win;
                driver.switchTo().window(win);
                break;
            }
        }

        if (newWindow == null) {
            driver.quit();
        }

        return new GCPCostEstimateSummary(driver,estimatedCost);

    }
}
