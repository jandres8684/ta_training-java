package com.epam.training.julian_naranjo.taskthree.test;

import com.epam.training.julian_naranjo.taskthree.page.GCPHomePage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GCPPricingCalculatorTest {
    private WebDriver driver;

    @Before
    public void browserSetup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void pricingCalculator() throws InterruptedException {

        boolean actualCostEstimatedSummary =
                new GCPHomePage(driver)
                        .openPage()
                        .gcpSelectOptionPricing()
                        .gcpSelectPricingCalculator()
                        .gcpInstanceConfigurationTotal().validateComputeEnginePriceSummaryPrice();

        Assert.assertTrue(actualCostEstimatedSummary);

    }

    @After
    public void closeBrowser(){
        driver.quit();
    }
}
